* Download the ["Amazon fine food reviews"](https://www.kaggle.com/snap/amazon-fine-food-reviews/downloads/amazon-fine-food-reviews.zip/2) dataset
* Extract files into the "dataset" folder so that all reviews are in this path "dataset/amazon-fine-food-reviews/Reviews.csv"

Run this example in the project root folder
```
gradle -PmainClass=edu.rit.cs.basic_word_count.WordCount_Seq execute
```