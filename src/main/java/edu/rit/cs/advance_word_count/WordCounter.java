package edu.rit.cs.advance_word_count;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The WordCounter thread class
 * the thread class for the WordCount_seq (thread version) class
 *
 * Contains all the information necessary to create a thread which will count the number of words in a given list of objects
 * containing words.
 */
public class WordCounter implements Runnable{

    private int start;                               //the starting index of the list

    private int end;                                 //the ending index of the list

    List<AmazonFineFoodReview> reviewList;           //a list containing Amazon fine food review objects

    Map<String, Integer> threadMap;                  //the finished map that the thread is sending back

    /**
     * The WordCounter thread object
     * Stores all the values a WordCounter thread needs/produces to be retrieved later
     * @param start the starting index of the list the thread is analyzing
     * @param end the ending index of the list the thread is analyzing
     * @param reviewList the list containing the sublist the thread will be analyzing
     */
    public WordCounter(int start, int end, List<AmazonFineFoodReview> reviewList){
        this.start = start;
        this.end = end;
        this.reviewList = reviewList;
    }

    /**
     * The run function for the thread
     * It will run through and count the words in its given sublist, store them in a map, and then store that map
     * internally
     */
    public void run() {
        List<String> words = new ArrayList<String>();
        for(int i = start; i<end; i++){//AmazonFineFoodReview review : this.reviewList) {
            AmazonFineFoodReview review = reviewList.get(i);
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                words.add(matcher.group().toLowerCase());
        }

//        /* Count words */
        Map<String, Integer> wordcount = new HashMap<>();
        for(String word : words) {
            if(!wordcount.containsKey(word)) {
                wordcount.put(word, 1);
            } else{
                int init_value = wordcount.get(word);
                wordcount.replace(word, init_value, init_value+1);
            }
        }
        this.threadMap = wordcount;
    }

    /**
     * Get function for the start value
     * @return the start value
     */
    public int getStart(){
        return this.start;
    }

    /**
     * Get function for the end value
     * @return the end value
     */
    public int getEnd(){
        return this.end;
    }

    /**
     * Get function for the list fed into the thread
     * @return the list fed into the thread
     */
    public List<AmazonFineFoodReview> getReviewList() {
        return reviewList;
    }

    /**
     * Get function for the thread generated map
     * @return the thread generated map
     */
    public Map<String, Integer> getThreadMap() {
        return threadMap;
    }
}
