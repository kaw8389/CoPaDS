package edu.rit.cs.advance_word_count;

import edu.rit.cs.MyTimer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import edu.rit.cs.advance_word_count.WordCounter;

/**
 * The WordCount Sequential class
 * Contains all the information necessary to count the words in the data file sequentially with threads
 */
public class WordCount_Seq {

    public static final String AMAZON_FINE_FOOD_REVIEWS_file="dataset/amazon-fine-food-reviews/Reviews.csv";
    /**
     * Reads in a dataset file and produces a list of AmazonFineFoodReviewObjects
     * @param dataset_file the file being read in
     * @return a list of AmazonFineFoodReview objects
     */
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * Pretty prints the map of counted words
     * @param wordcount the map of counted words
     */
    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }
    /**
     * The main thread, reads in a dataset file then counts the words in that dataset file by dividing the file
     * into threads and then having each individual thread cound words in its given patrician passing back an
     * unsorted map of counted words.
     * At the end it orders the counted maps of words by merging them all into one map and remapping it to a treemap
     * @param args the arguments
     */
    public static void main(String[] args){
        System.out.println("Scanning file...");
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);
        System.out.println("Finished File Read...");
        System.out.println("Starting threading...");
        int chunksize = 20;
        int Listsize = allReviews.size()-1;
        int partsize = Listsize/chunksize;
        /* For debug purpose */
//        for(AmazonFineFoodReview review : allReviews){
//            System.out.println(review.get_Text());
//        }
        List<WordCounter> tasklist = new ArrayList<WordCounter>();
        for(int i =0; i<chunksize; i++) {
            if (i == chunksize){

            }else{
                tasklist.add(new WordCounter(i*partsize,((i+1)*partsize), allReviews));
            }
        }
        List<Thread> threadList = new ArrayList<Thread>();

        MyTimer myTimer = new MyTimer("wordCount");
        myTimer.start_timer();
        /* Tokenize words */
        for(int i =0; i<chunksize; i++) {
            if (i > chunksize){

            }else{
                threadList.add(new Thread(tasklist.get(i)));
                threadList.get(i).start();
            }
        }
        for(int i =0; i<chunksize; i++){
            try{
                threadList.get(i).join();
            }catch(Exception ex){
                System.out.println("Thread join exception");
            }
        }
//        /* Count words */
        Map<String, Integer> wordcount = new HashMap<>();
        for(int i = 0; i<chunksize; i++){
            Map<String, Integer> map1 = tasklist.get(i).getThreadMap();
            map1.forEach((k, v) -> wordcount.merge(k, v, (v1, v2) -> v1+v2));
        }
        Map<String, Integer> sortedcount = new TreeMap<String, Integer>(wordcount);
        myTimer.stop_timer();
        
        print_word_count(sortedcount);

        myTimer.print_elapsed_time();
    }

}