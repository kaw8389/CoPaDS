package edu.rit.cs.ClientServer;

import edu.rit.cs.ClientServer.AmazonFineFoodReview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordCounter implements Runnable{
    private int start;
    private int end;
    List<AmazonFineFoodReview> reviewList;
    Map<String, Integer> threadMap;


    public WordCounter(int start, int end, List<AmazonFineFoodReview> reviewList){
        this.start = start;
        this.end = end;
        this.reviewList = reviewList;
    }

    public void run() {
        List<String> words = new ArrayList<String>();
        for(int i = start; i<end; i++){//AmazonFineFoodReview review : this.reviewList) {
            AmazonFineFoodReview review = reviewList.get(i);
            Pattern pattern = Pattern.compile("([a-zA-Z]+)");
            Matcher matcher = pattern.matcher(review.get_Summary());

            while(matcher.find())
                words.add(matcher.group().toLowerCase());
        }

//        /* Count words */
        Map<String, Integer> wordcount = new HashMap<>();
        for(String word : words) {
            if(!wordcount.containsKey(word)) {
                wordcount.put(word, 1);
            } else{
                int init_value = wordcount.get(word);
                wordcount.replace(word, init_value, init_value+1);
            }
        }
        this.threadMap = wordcount;
    }
    public int getStart(){return this.start;}
    public int getEnd(){return this.end;}

    public List<AmazonFineFoodReview> getReviewList() {
        return reviewList;
    }

    public Map<String, Integer> getThreadMap() {
        return threadMap;
    }
}
