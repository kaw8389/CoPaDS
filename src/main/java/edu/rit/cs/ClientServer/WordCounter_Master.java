package edu.rit.cs.ClientServer;

import edu.rit.cs.ClientServer.AmazonFineFoodReview;
import edu.rit.cs.ClientServer.WordCounter;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.net.*;
import java.io.*;


/**
 * The WordCounter_Master Class
 * Defines all the information necessary to create a
 * WordCounter Server for a given dataset
 */
public class WordCounter_Master {
    public static final String AMAZON_FINE_FOOD_REVIEWS_file="dataset/amazon-fine-food-reviews/Reviews.csv.bak";
    /**
     * Reads in a dataset file and produces a list of AmazonFineFoodReviewObjects
     * @param dataset_file the file being read in
     * @return a list of AmazonFineFoodReview objects
     */
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * The server code for the ClientServer network code
     * reads in a datafile Reviews.csv.bak and then partitions it
     * out to various containers which then process it, and send back a sorted map.
     * It then takes the sorted maps from each container and combines them into a single
     * sorted map, then prints that map.
     */
    public static void Server(){
        //Inital File Read
        System.out.println("Scanning File..");
        List<AmazonFineFoodReview> allReviews = read_reviews(AMAZON_FINE_FOOD_REVIEWS_file);
        System.out.println("File Scanned.");
        System.out.println("Beginning Partitioning started...");
        //List of Connections initialization
        List<Connection> connectionList = new ArrayList<Connection>();
        //Treemap initialization
        TreeMap<String, Integer> FINALMAP = new TreeMap<>();
        //variables
        int chunksize = 3; //number of file partitions
        int filesleft = chunksize; //number of files left
        int Listsize = allReviews.size()-1; //total number of entries in the list
        int partsize = Listsize/chunksize; //the size of each individual partition
        int fileindex = 0; //the current file partition being looked at
        //List of file partitions
        List<List<AmazonFineFoodReview>> FileList = new ArrayList<>();
        //partion the data into chunks
        for(int i =0; i<chunksize; i++){
            if((i+1)*partsize > Listsize){
            }else{
                FileList.add(new ArrayList<AmazonFineFoodReview>(allReviews.subList(i*partsize,((i+1)*partsize))));
                System.out.println("Partitioned...");
            }
        }
        List<AmazonFineFoodReview> FileSent = FileList.get(fileindex);
        try{
            int serverPort = 7896;
            ServerSocket listenSocket = new ServerSocket(serverPort);
            System.out.println("Server started");
            List<Socket> socketList = new ArrayList<>();
            int socketindex = 0;
            //Connection code///////////////////////////////////////////////////////
            while(filesleft>0) {
                System.out.println("Waiting for a client ...");
                socketList.add(listenSocket.accept());
                System.out.println("Client accepted");
                if(fileindex<= FileList.size()){
                    FileSent = FileList.get(fileindex);
                    Connection c = new Connection(socketList.get(socketindex), FileSent, filesleft);
                    connectionList.add(c);
                    System.out.println("Established Connection accepted; File sent.");
                    socketindex++;
                    fileindex++;
                    filesleft--;
                    System.out.println("Files remaining: "+filesleft);
                }else{
                    System.out.println("No files left to send; Connection ignored.");
                }
            }
            //End of connection code////////////////////////////////////////////////
            //
            //Map joining code//////////////////////////////////////////////////////
            for(int i =0; i<chunksize; i++){
                try{
                    connectionList.get(i).join();
                }catch(Exception ex){
                    System.out.println("Thread join exception");
                }
            }
            Map<String, Integer> wordcount = new HashMap<>();
            for(int i = 0; i<chunksize-1; i++){
                Map<String, Integer> map1 = connectionList.get(i).getThreadmap();
                map1.forEach((k, v) -> wordcount.merge(k, v, (v1, v2) -> v1+v2));
            }
            Map<String, Integer> sortedcount = new TreeMap<String, Integer>(wordcount);
            print_word_count(sortedcount);
            //End of map joining code///////////////////////////////////////////////
        } catch(IOException e) {
            e.printStackTrace();
            System.out.println("Listen :"+e.getMessage());}
    }

    /**
     * main thread calls the Server thread
     * @param args the arguments
     */
    public static void main (String args[]) {
            Server();
        }

    /**
     * Pretty prints a sorted map
     * @param wordcount the sorted map
     */
    public static void print_word_count( Map<String, Integer> wordcount){
        for(String word : wordcount.keySet()){
            System.out.println(word + " : " + wordcount.get(word));
        }
    }
}

/**
 * The class which defines a connection object
 *
 * A connection object is the object which stores the connection information between
 * the server socket and the client socket
 */
class Connection extends Thread {
    DataInputStream in;                      //the data input stream
    DataOutputStream out;                    //the data output stream
    Socket clientSocket;                     //the client socket
    OutputStream os;                         //the normal output stream
    ObjectOutputStream objectOutput;         //the object output stream
    TreeMap<String, Integer> Threadmap;      // the Sorted map sent back to each connection

    /**
     * The connection object
     * @param aClientSocket the client socket
     * @param FileSent the file partion to be sent through the client socket
     * @param filesleft the number of files remaining in the main server thread
     */
    public Connection (Socket aClientSocket, List<AmazonFineFoodReview> FileSent, int filesleft) {
        try {
            clientSocket = aClientSocket;
            in = new DataInputStream( clientSocket.getInputStream());
            out =new DataOutputStream( clientSocket.getOutputStream());
            //////////////////////////////////////////////
            //checks to see if clientsocket excists then sends out
            //the partitioned data to the client thread
            //////////////////////////////////////////////
            if(clientSocket != null && filesleft>=0){
                //output streams
                ByteArrayOutputStream ba = new ByteArrayOutputStream();
                os = clientSocket.getOutputStream();
                objectOutput = new ObjectOutputStream(os);
                //end of outputstreams
                System.out.println("Sending File:" + filesleft + " Length:" + FileSent.size() + " bytes...");
                objectOutput.writeObject(FileSent);
                byte[] bytes = ba.toByteArray();
                os.write(bytes);
                os.flush();
                System.out.println("File Successfully sent.");
                //closing this closes the socket
            }
            //////////////////////////////////////////////////
            //Starts the thread
            /////////////////////////////////////////////////
            this.start();
        } catch(IOException e)  {
            e.printStackTrace();
            System.out.println("Connection:"+e.getMessage());}
    }

    public void run(){
        int status = 0;
        TreeMap<String, Integer> RecievetreeMap = new TreeMap<>();
        try {
            ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
            ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
            ////////////////////////
            //while the thread is unfished always read for client input
            ////////////////////////
            while (status == 0){
                //The connections will ALWAYS be sending a treemap back
                @SuppressWarnings(value = "unchecked")
                TreeMap<String, Integer> Recieverealmap = (TreeMap<String, Integer>) ois.readObject();
                RecievetreeMap = Recieverealmap;
                status = 1;
            }
            Threadmap = RecievetreeMap;
        } catch(EOFException e) {
            System.out.println("EOF:"+e.getMessage());
        } catch(IOException e) {
            e.printStackTrace();
            System.out.println("IO:"+e.getMessage());
        } catch(ClassNotFoundException e){
            e.printStackTrace();
        }finally{
            //Close all sockets
            try {
                if(os != null){
                    os.close();
                }
                if(objectOutput != null){
                    objectOutput.close();
                }
                if(clientSocket != null){
                    clientSocket.close();
                }
            }catch (IOException e){
                e.printStackTrace();
                /*close failed*/
            }
        }
    }
    public TreeMap<String, Integer> getThreadmap() {
        return Threadmap;
    }
}
