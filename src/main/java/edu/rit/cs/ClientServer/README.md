Readme for the Client Server Package 
_____________________________________

In order to run the client server code you must run the following command on the Reviews.csv file in the dataset folder: 
    
    mv Reviews.csv Reviews.csv.bak; head -n 3000 Reviews.csv.bak > Reviews.csv cd WordCount/CoPaDS/src/main/java
                            
Additionally the dataset directory must be located in the directory you are invoking the WordCount_Master thread in, 
in this case it would have to be located in CoPaDS/src/main/java
_____________________________________

You must  call the WordCounter_master function as such: 
    
    java -Xmx1024m edu.rit.cs.ClientServer.WordCounter_Master

To ensure that the Master server has enough heap memory to read in the full datset file. 

The Client should be called as: 
    
    java edu.rit.cs.ClientServer.WordCounter_Worker <ipaddress>
______________________________________


The client server package is meant to be run in a specific order. The WordCounter_Master thread needs to be started first 
as it functions as the networks main server. After the WordCounter_Master thread is fully started and displays "Waiting for a Client..."
then and only then should Client nodes begin trying to connect to the server. Any connections prior to this point will be rejected as the server will
not have opened any sockets to accept connections. When the program is finished running it will display the full sorted map.