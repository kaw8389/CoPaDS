package edu.rit.cs.ClientServer;

import edu.rit.cs.ClientServer.AmazonFineFoodReview;
import edu.rit.cs.ClientServer.WordCounter;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

/**
 * The WordCounter_Worker Class
 * Defines all the information neccessary to create
 * a WordCounter client node
 */
public class WordCounter_Worker {
    /**
     * Reads in a dataset file and produces a list of AmazonFineFoodReviewObjects
     * @param dataset_file the file being read in
     * @return a list of AmazonFineFoodReview objects
     */
    public static List<AmazonFineFoodReview> read_reviews(String dataset_file) {
        List<AmazonFineFoodReview> allReviews = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(dataset_file))){
            String reviewLine = null;
            // read the header line
            reviewLine = br.readLine();

            //read the subsequent lines
            while ((reviewLine = br.readLine()) != null) {
                allReviews.add(new AmazonFineFoodReview(reviewLine));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return allReviews;
    }

    /**
     * The main thread
     * @param args the arguments who's 0th value should be the ip address
     *             of the master server.
     */
    public static void main (String args[]) {
        client(args[0]);
    }

    /**
     * The client node
     * @param ipadress the ip address of the master server
     */
    public static void client(String ipadress){
        Socket s = null;
        try{
            int serverPort = 7896;
            //
            //socket
            System.out.println("Connecting...");
            s = new Socket(ipadress, serverPort);
            System.out.println("Connected...");

            //
            //data input stream
            InputStream input = s.getInputStream();
            DataInputStream in = new DataInputStream(input);
            ObjectInputStream ois = new ObjectInputStream(input);
            //
            //dataoutputstream
            DataOutputStream out = new DataOutputStream(s.getOutputStream());
            ObjectOutputStream mapsender = new ObjectOutputStream(s.getOutputStream());
            ////////////////////////////////////////////////////
            //File writer code
            //Recieves file from master server
            ////////////////////////////////////////////////////
            //The outputstream will ALWAYS be sending a List of amazonfinefoodreviews to this thread
            ////////////////////////////////////////////////////
            System.out.println("Scanning file...");
            System.out.println("Starting threading...");
            @SuppressWarnings(value = "unchecked")
            List<AmazonFineFoodReview> allReviews = (List<AmazonFineFoodReview>) ois.readObject();
            System.out.println("List Read...");
            ////////////////////////////////////////////////////
            //end File Writer code
            ////////////////////////////////////////////////////
            //WordCounter Code
            //Divides the partion recieved into seperate threads and then processes
            ////////////////////////////////////////////////////
            int chunksize = 5;
            int Listsize = allReviews.size()-1;
            int partsize = Listsize/chunksize;

            List<WordCounter> tasklist = new ArrayList<WordCounter>();
            for(int i =0; i<chunksize; i++) {
                if (i == chunksize){

                }else{
                    tasklist.add(new WordCounter(i*partsize,((i+1)*partsize), allReviews));
                }
            }
            List<Thread> threadList = new ArrayList<Thread>();

            /* Tokenize words */
            for(int i =0; i<chunksize; i++) {
                if (i > chunksize){

                }else{
                    threadList.add(new Thread(tasklist.get(i)));
                    threadList.get(i).start();
                }
            }
            for(int i =0; i<chunksize; i++){
                try{
                    threadList.get(i).join();
                }catch(Exception ex){
                    System.out.println("Thread join exception");
                }
            }
//        /* Count words */
            Map<String, Integer> wordcount = new HashMap<>();
            for(int i = 0; i<chunksize; i++){
                Map<String, Integer> map1 = tasklist.get(i).getThreadMap();
                map1.forEach((k, v) -> wordcount.merge(k, v, (v1, v2) -> v1+v2));
            }
            Map<String, Integer> sortedcount = new TreeMap<String, Integer>(wordcount);
            /////////////////////////////////////////
            //EndWordCounterCode
            /////////////////////////////////////////
            //Send Client code
            //Sends the sorted map back to the master server
            /////////////////////////////////////////
            mapsender.writeObject(sortedcount);
            System.out.println("Sent sorted map...");
            /////////////////////////////////////////
            //End Send Client Code
            /////////////////////////////////////////

        }catch (UnknownHostException e){
            System.out.println("Sock:"+e.getMessage());
        }catch (EOFException e){
            System.out.println("EOF:"+e.getMessage());
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("IO:"+e.getMessage());
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}
